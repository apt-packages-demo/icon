# icon

Icon Programming Language. https://www2.cs.arizona.edu/icon

## Main features of Icon and Unicon after a few tests
* Nice syntax
* Starts fast
* Included libraries for usual tasks
* No C/C++ interface!

## Icon influenced languages
* Goaldi: A Goal-Directed Programming Language
  * [proebsting/goaldi](https://github.com/proebsting/goaldi)